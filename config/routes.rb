Rails.application.routes.draw do
  resources :vehicles do
    get 'inventory', to: 'vehicles#inventory', on: :collection
  end
  root 'vehicles#index'
end
