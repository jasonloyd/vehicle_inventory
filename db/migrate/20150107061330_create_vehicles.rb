class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :vehicle_make
      t.string :vehicle_model
      t.integer :vehicle_year
      t.integer :dept_number
      t.string :updated_by
      t.datetime :last_updated
      t.integer :mileage
      t.boolean :checked_in

      t.timestamps null: false
    end
  end
end
