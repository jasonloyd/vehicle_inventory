# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
#  create_table "vehicles", force: :cascade do |t|
#    t.string   "vehicle_make"
#    t.string   "vehicle_model"
#    t.integer  "vehicle_year"
#    t.integer  "dept_number"
#    t.string   "updated_by"
#    t.datetime "last_updated"
#    t.integer  "mileage"
#    t.boolean  "checked_in"
#    t.datetime "created_at",    null: false
#    t.datetime "updated_at",    null: false
#  end

Vehicle.where(vehicle_make: "GMC", vehicle_model: "Sierra", vehicle_year: 2015, dept_number: 123, updated_by: "Jason Loyd", last_updated: Time.now, mileage: 550, checked_in: true).first_or_create
Vehicle.where(vehicle_make: "Chevrolet", vehicle_model: "Savana", vehicle_year: 2015, dept_number: 123, updated_by: "Jason Loyd", last_updated: Time.now, mileage: 450, checked_in: true).first_or_create
Vehicle.where(vehicle_make: "Chevrolet", vehicle_model: "Suburban", vehicle_year: 2012, dept_number: 321, updated_by: "Jason Loyd", last_updated: Time.now, mileage: 23200, checked_in: true).first_or_create
Vehicle.where(vehicle_make: "Chevrolet", vehicle_model: "Suburban", vehicle_year: 2013, dept_number: 321, updated_by: "Jason Loyd", last_updated: Time.now, mileage: 12200, checked_in: true).first_or_create
Vehicle.where(vehicle_make: "Ford", vehicle_model: "F-250", vehicle_year: 2010, dept_number: 321, updated_by: "Jason Loyd", last_updated: Time.now, mileage: 58888, checked_in: true).first_or_create
