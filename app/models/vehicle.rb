class Vehicle < ActiveRecord::Base
  def self.format_inventory
    inventory = {}
    all_vehicles = Vehicle.all
    inventory["resultCount"] = all_vehicles.count
    inventory["results"] = []
    all_vehicles.each do |v|
      inventory["results"] << v
    end
    inventory
  end
end
