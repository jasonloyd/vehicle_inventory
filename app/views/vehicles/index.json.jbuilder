json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :vehicle_make, :vehicle_model, :vehicle_year, :dept_number, :updated_by, :last_updated, :mileage, :checked_in
  json.url vehicle_url(vehicle, format: :json)
end
